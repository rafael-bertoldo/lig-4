//criar a tabela
const table = [
    '111111',
    '111111',
    '111111',
    '111111',
    '111111',
    '111111',
    '111111'
]
const verify = [
    [5, 4, 3, 2, 1, 0],
    [11, 10, 9, 8, 7, 6],
    [17, 16, 15, 14, 13, 12],
    [23, 22, 21, 20, 19, 18],
    [29, 28, 27, 26, 25, 24],
    [35, 34, 33, 32, 31, 30],
    [41, 40, 39, 38, 37, 36]
]

const modalMsg = document.getElementById("msgModal")
const btnRed = document.getElementById('btnRed')
const btnBlue = document.getElementById('btnBlue')
const modalBtn = document.createElement("button")
const modal = document.getElementById('modal')
btnRed.classList.add('playerNeonRed')

const container = document.getElementById('container')
const columns = document.getElementsByClassName('columnContainer')
const cells = []
let count = 0
let verifyDrow = 0

// sounds
const newGameSound = new Audio('./assets/audio/NewGame.wav');
const addRingSound = new Audio('./assets/audio/AddRing2.wav');
const winSound = new Audio('./assets/audio/Win.wav')
addRingSound.volume = 0.5;
newGameSound.volume = 0.5;
winSound.volume = 0.25;


function makeTable() {
    for (let i = 0; i < table.length; i++) {
        const column = document.createElement('div')
        column.classList.add('columnContainer')
        column.id = `column${i}`
        container.appendChild(column)
        for (let j = 0; j < table[i].length; j++) {
            const cell = document.createElement('div')
            cell.classList.add('cell')
            column.appendChild(cell)
        }

    }
}
makeTable()

getCells()

function getCells() {
    for (let i in columns) {
        columns[i].childNodes !== undefined ? cells.push(columns[i].childNodes) : false;
    }
}

function createPiece(item, c, i) {
    const piece = document.createElement('div')
    if (count === 0) {
        piece.classList.add('pieceRed')
        btnBlue.classList.add('playerNeonBlue')
        btnRed.classList.remove('playerNeonRed')
        count = 1
        verify[c][i] = "R"
        verifyDrow++
    } else {
        piece.classList.add('pieceBlue')
        btnRed.classList.add('playerNeonRed')
        btnBlue.classList.remove('playerNeonBlue')
        count = 0
        verify[c][i] = "B"
        verifyDrow++
    }
    item.appendChild(piece)
    return piece
}

container.addEventListener('click', (event) => {
    addRingSound.currentTime = 0
    addRingSound.play()
    for (let i in cells) {
        let currentEvent = cells[i]
        for (let j = 0; j < currentEvent.length; j++) {
            if (currentEvent[j] === event.target) {
                putPiece(currentEvent, i)
            }
        }
    }
    isLig4()
})

function putPiece(item, c) {
    for (let i = item.length - 1; i >= 0; i--) {
        if (item[i].childNodes.length === 0) {
            createPiece(item[i], c, i)
            return true
        } else if (item[0].childNodes.length !== 0) {
            return false
        }
    }
}

function isLig4() {
    for (let i = 0; i < verify.length; i++) {
        for (let j = 0; j < verify[i].length - 3; j++) {
            if (verify[i][j] === verify[i][j + 1]) {
                if (verify[i][j] === verify[i][j + 2]) {
                    if (verify[i][j] === verify[i][j + 3]) {
                        getSequency(i,i,i,i,j,j+1,j+2,j+3,verify[i][j])
                        getWinner(verify[i][j])
                    }
                }
            }
        }
    }
    for (let i = 0; i < verify.length - 3; i++) {
        for (let j = 0; j < verify[i].length; j++) {
            if (verify[i][j] === verify[i + 1][j]) {
                if (verify[i][j] === verify[i + 2][j]) {
                    if (verify[i][j] === verify[i + 3][j]) {
                        getSequency(i,i+1,i+2,i+3,j,j,j,j,verify[i][j])
                        getWinner(verify[i][j])
                    }
                }
            }
        }
    }
    for (let i = 0; i < verify.length - 3; i++) {
        for (let j = 0; j < verify[i].length - 3; j++) {
            if (verify[i][j] === verify[i + 1][j + 1]) {
                if (verify[i][j] === verify[i + 2][j + 2]) {
                    if (verify[i][j] === verify[i + 3][j + 3]) {
                        getSequency(i+1,i+2,i+3,i,j+1,j+2,j+3,j,verify[i][j])
                        getWinner(verify[i][j])
                    }
                }
            }
        }
    }
    for (let i = verify.length - 1; i >= 3; i--) {
        for (let j = 0; j < verify[i].length - 3; j++) {
            if (verify[i][j] === verify[i - 1][j + 1]) {
                if (verify[i][j] === verify[i - 2][j + 2]) {
                    if (verify[i][j] === verify[i - 3][j + 3]) {
                        getSequency(i-1,i-2,i-3,i,j+1,j+2,j+3,j,verify[i][j])
                        getWinner(verify[i][j])
                    }
                }
            }
        }
    }
    if (verifyDrow === 42){
        setTimeout(() => {
            createModal()
        }, 1000); 
    }
   
}

// Função de reset game

const resetGame = () => {
    newGameSound.play()
    let reset = document.getElementsByClassName('cell')
    for (let i = 0; i < reset.length; i++) {
        let columns = reset[i];
        columns.innerHTML = ''
    }

    verifyDrow = 0;
    let countJ = 0;

    for (let i = 0; i < verify.length; i++) {
        for (let j = verify[i].length - 1; j >= 0; j--) {
            if (verify[i][j] !== countJ) {
                verify[i][j] = countJ;
                countJ++
            } else {
                countJ++
            }
        }
    }
    for (let i in cells){
        for (let j = 0; j < cells[i].length; j++){
            cells[i][j].classList.remove("fill_sequency_red")
            cells[i][j].classList.remove("fill_sequency_blue")
        }
    }
    
    modal.classList.add('hidden')
    container.classList.remove("none_event_click")
}


function getWinner(winner) {
    if (winner === "R") {
        modalMsg.textContent = "RED WINS"
        modalMsg.classList.remove('blue_winner')
        modalMsg.classList.remove('draw')
        modalMsg.classList.add("red_winner")
        count = 1

    } else if (winner === 'B') {
        modalMsg.textContent = "BLUE WINS"
        modalMsg.classList.remove('red_winner')
        modalMsg.classList.remove('draw')
        modalMsg.classList.add("blue_winner")
        count = 0
    }
    container.classList.add("none_event_click")
    setTimeout(() => {
        createModal()
    }, 1000); 
}

function createModal(){
    modal.classList.remove('hidden')
    modalBtn.id = "modal-button"
    modalBtn.innerText = "NEW GAME"
    modalMsg.appendChild(modalBtn)
    if (verifyDrow === 42) {
        modalMsg.textContent = 'DRAW'
        modalMsg.classList.remove('red_winner')
        modalMsg.classList.remove('blue_winner')
        modalMsg.classList.add('draw')
        modal.classList.remove('hidden')
        modalBtn.id = "modal-button"
        modalBtn.innerText = "NEW GAME"
        modalMsg.appendChild(modalBtn)
    }
    winSound.play() 
}

function getSequency(column1,column2,column3,column4,row1,row2,row3,row4,color){
    if (color === "R"){
        cells[column1][row1].classList.add("fill_sequency_red")
        cells[column2][row2].classList.add("fill_sequency_red")
        cells[column3][row3].classList.add("fill_sequency_red")
        cells[column4][row4].classList.add("fill_sequency_red")
    }
    if (color === "B"){
        cells[column1][row1].classList.add("fill_sequency_blue")
        cells[column2][row2].classList.add("fill_sequency_blue")
        cells[column3][row3].classList.add("fill_sequency_blue")
        cells[column4][row4].classList.add("fill_sequency_blue")
    }
}
modalBtn.addEventListener('click', resetGame)
